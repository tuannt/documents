## Cấu hình S3 Browser

![](https://3502414041-files.gitbook.io/~/files/v0/b/gitbook-x-prod.appspot.com/o/spaces%2FxPwcikmdPwnpsZqRavYs%2Fuploads%2Fg3lTcADLbaefMxUF1roY%2Fimage.png?alt=media&token=510cadc6-518e-49f8-8818-a1d855d06b0b)

### Nhập thông tin kết nối

Sử dụng Menu tab để quản ý Add new account, Manage account ... (với Free version tối đa chỉ thêm được 2 accounts)

![](https://3502414041-files.gitbook.io/~/files/v0/b/gitbook-x-prod.appspot.com/o/spaces%2FxPwcikmdPwnpsZqRavYs%2Fuploads%2FNaEFRNC0WZ2foz1S5sPQ%2Fimage.png?alt=media&token=356861ac-8edd-42da-8423-c0bbc901017e)


![](https://3502414041-files.gitbook.io/~/files/v0/b/gitbook-x-prod.appspot.com/o/spaces%2FxPwcikmdPwnpsZqRavYs%2Fuploads%2FiADxl0wUl7IpGaSeVEaw%2Fimage.png?alt=media&token=bb8aa998-eda3-4e99-92c0-8e772319c5b0)

![](https://3502414041-files.gitbook.io/~/files/v0/b/gitbook-x-prod.appspot.com/o/spaces%2FxPwcikmdPwnpsZqRavYs%2Fuploads%2FeMJyL1ObLLsLTIB9Cv7S%2Fconnect.png?alt=media&token=a552930d-3196-48d4-a8c5-7dcc579772be)

### Kết nối thành công và tạo Bucket để lưu dữ liệu

![](https://3502414041-files.gitbook.io/~/files/v0/b/gitbook-x-prod.appspot.com/o/spaces%2FxPwcikmdPwnpsZqRavYs%2Fuploads%2FIiDzvUI2vZWZSiIO8K5d%2Fimage.png?alt=media&token=f8da3111-caf4-4362-8346-9b94fb5e398a)

### Có thể phân quyền cho Bucket hay các file

![](https://3502414041-files.gitbook.io/~/files/v0/b/gitbook-x-prod.appspot.com/o/spaces%2FxPwcikmdPwnpsZqRavYs%2Fuploads%2FIUFOEYT2HGNxa5GkMqHf%2Fview1.png?alt=media&token=be0f634f-afc7-4e2c-b13b-7fb3e41ce78e)


> Dùng các chức năng trên thanh công cụ để thao tác upload và download dữ liệu. Có thể phân quyền truy cập các file, folder, bucket bằng cách: chọn các ô tương ứng quyền truy cập hoặc chọn file, folder, bucket rồi chọn Makepublic -> Apply changes -> Reload

![](https://3502414041-files.gitbook.io/~/files/v0/b/gitbook-x-prod.appspot.com/o/spaces%2FxPwcikmdPwnpsZqRavYs%2Fuploads%2FKl0fKJMw0ICjJSHDjyta%2Fshare.png?alt=media&token=1a2b11f5-7256-4e4e-a6d5-b0f594b9bd0a)

> Mặc định ứng dụng S3 Browser sẽ tạo đường dẫn đến files: https://<bucket-name>.domain/<folder>/file

> Nguồn từ hướng dẫn từ fstorage
