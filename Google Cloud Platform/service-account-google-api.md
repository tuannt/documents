## Các điều kiện cần có trước khi tạo service account GCP

- Đã đăng nhập một tài khoản Google
- Đã tạo 1 project trong Google Cloud Platform

## Bước 1: Truy cập vào trang Google Cloud Platform 
![](https://tuanntblog.com/wp-content/uploads/2022/05/GCP-1.png)

- Link: https://console.cloud.google.com/
- Chọn **IAM & Admin**  
- Click chọn **Service Account**

## Bước 2: Click tạo Account Service
![](https://tuanntblog.com/wp-content/uploads/2022/05/GCP-2.png)

- Click vào nút **+ CREATE SERVICE ACCOUNT**

## Bước 3: Điền thông tin chi tiết của Account Service

![](https://tuanntblog.com/wp-content/uploads/2022/05/Create-service-account-–-IAM-Admin-–-Verygirlie-–-Google-Cloud-Platform.png)

- Nhập tên cho account service
- Nhập mô tả cho account service
- Click button **CREATE AND CONTINUE**

## Bước 4: Cấp quyền cho Account Service (tùy chọn)

![](https://tuanntblog.com/wp-content/uploads/2022/05/Grant-service-account-–-IAM-Admin-–-Verygirlie-–-Google-Cloud-Platform.png)

---

![](https://tuanntblog.com/wp-content/uploads/2022/05/ROLE-IAM.png)

---

![](https://tuanntblog.com/wp-content/uploads/2022/05/Done-Create-service-account-–-IAM-Admin-–-Verygirlie-–-Google-Cloud-Platform.png)

---

- Click chọn **ADD ROLE**
- Chọn **Currentlly used** → **Owner**
- Click **Done**

---

## Bước 5: Tạo key

![](https://tuanntblog.com/wp-content/uploads/2022/05/managekyes.png)

- Click vào dấm 3 chấm đứng: Action
- Chọn Manage Keys

---

![](https://tuanntblog.com/wp-content/uploads/2022/05/tuannt-service-account-–-IAM-Admin-–-Verygirlie-–-Google-Cloud-Platform.png)

- Click **ADD KEY**
- Chọn **Create new key**

---

![](https://tuanntblog.com/wp-content/uploads/2022/05/craetekeyjson-tuannt-service-account-–-IAM-Admin-–-Verygirlie-–-Google-Cloud-Platform.png)

- Key Type: JSON
- Click **CREATE**

Sau khi nhấn vào nút **create** đồng thời chúng ta cũng có 1 file json được tải về, và đấy chính là file chứa thông tin private_key giúp chúng ta có thể sử dụng các api google thông qua file json đó.

Nếu có thắc mắc hoặc có gì khó hiểu thì các bạn để lại bình luận bên dưới mình sẽ giải đáp thắc mắc nhanh nhất có thể nhé.

> Nguồn bài viết từ: https://tuanntblog.com/service-account-google-api/

